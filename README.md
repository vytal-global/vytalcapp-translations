# @vytal/consumer-app-translations

For more info, [read this](https://vytalist.atlassian.net/wiki/spaces/VD/pages/8290402/i18n).

To add, delete or update translations, navigate to `translations.json` and click Edit at the top right of the file.

⚠️ Any edit you make **will go straight to production**, so be super careful and follow these rules:

- update both en and de translations
- syntax: make sure all items are wrapped in `“”`
- syntax: don’t forget a coma `,` between two lines, **except** if you’re on the last line of a block
- **use the preview tool** ("Preview changes" at the top left of the file) to see what you changed before saving
- if you’re deleting or renaming items, check with the dev team to make sure it won't break things in the app

Most importantly, have fun! 🎉
